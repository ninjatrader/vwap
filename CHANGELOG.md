Changelog
=========

1.0.2
-----

* enabled realtime calculation (CalculateOnBarClose: false)


1.0.1
-----

* removed collections/linq
* removed period
* way faster now!


1.0.0
-----

* initial release
