#region License and Terms
/*
The MIT License (MIT)

Copyright (c) 2013-2014 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#endregion

#region Using declarations
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Gui.Design;
#endregion Using declarations

namespace NinjaTrader.Indicator {
	[Description("Vitalij's VWAP indicator for NinjaTrader 7" + "\r\n" + "http://ninjatrader.bitbucket.org/")]
	public class VitVWAP : Indicator {

		#region Variables
		double lv = 0; // last volume
		double lp = 0; // last average price * volume
		double cv = 0; // current volume
		double cp = 0; // current average price * volume

		DashStyle dashStyle = DashStyle.Dot;
		int width = 3;
		Color colorUp = Color.LimeGreen;
		Color colorDown = Color.Red;
		#endregion

		protected override void Initialize() {
			Overlay = true;
			PlotsConfigurable = false;

			Name = "Vitalij's VWAP Indicator";

			Add(new Plot(Color.Black, "vwap"));
		}

		protected override void OnStartUp() {
			Plots[0].Pen.DashStyle = dashStyle;
			Plots[0].Pen.Width = width;
		}

		protected override void OnBarUpdate() {
			if (FirstTickOfBar) {
				lv = cv;
				lp = cp;
			}

			if (Bars.FirstBarOfSession) {
				lv = 0;
				lp = 0;

				PlotColors[0][0] = Color.Transparent;
			} else if (CurrentBar > 0) {
				PlotColors[0][0] = (Values[0][1] < Values[0][0]) ? colorUp : colorDown;
			}

			cv = lv + Volume[0];
			cp = lp + (High[0] + Low[0] + Open[0] + Close[0]) / 4 * Volume[0];

			Values[0].Set(cp / cv);
		}

		private T ParseEnum<T>(string value) {
			return (T)Enum.Parse(typeof(T), value, true);
		}

		#region Properties
		[XmlIgnore()]
		[NinjaTrader.Gui.Design.DisplayName("01. Dash Style")]
		public DashStyle DashStyle {
			get { return dashStyle; }
			set { dashStyle = value; }
		}

		[Browsable(false)]
		public string DashStyleSerialize {
			get { return dashStyle.ToString(); }
			set { dashStyle = ParseEnum<DashStyle>(value); }
		}

		[Gui.Design.DisplayName("02. Width")]
		public int Width {
			get { return width; }
			set { width = Math.Max(value, 1); }
		}

		[XmlIgnore()]
		[Gui.Design.DisplayName("03. Up Color")]
		public Color ColorUp {
			get { return colorUp; }
			set { colorUp = value; }
		}

		[Browsable(false)]
		public string ColorUpSerialize {
			get { return SerializableColor.ToString(colorUp); }
			set { colorUp = SerializableColor.FromString(value); }
		}

		[XmlIgnore()]
		[Gui.Design.DisplayName("04. Down Color")]
		public Color ColorDown {
			get { return colorDown; }
			set { colorDown = value; }
		}

		[Browsable(false)]
		public string ColorDownSerialize {
			get { return SerializableColor.ToString(colorDown); }
			set { colorDown = SerializableColor.FromString(value); }
		}
		#endregion

	}
}

#region NinjaScript generated code. Neither change nor remove.
namespace NinjaTrader.Indicator {
	public partial class Indicator : IndicatorBase {
		private VitVWAP[] cacheVitVWAP = null;

		private static VitVWAP checkVitVWAP = new VitVWAP();

		public VitVWAP VitVWAP() {
			return VitVWAP(Input);
		}

		public VitVWAP VitVWAP(Data.IDataSeries input) {
			if (cacheVitVWAP != null)
				for (int idx = 0; idx < cacheVitVWAP.Length; idx++)
					if (cacheVitVWAP[idx].EqualsInput(input))
						return cacheVitVWAP[idx];

			lock (checkVitVWAP) {
				if (cacheVitVWAP != null)
					for (int idx = 0; idx < cacheVitVWAP.Length; idx++)
						if (cacheVitVWAP[idx].EqualsInput(input))
							return cacheVitVWAP[idx];

				VitVWAP indicator = new VitVWAP();
				indicator.BarsRequired = BarsRequired;
				indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
				indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
				indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
				indicator.Input = input;
				Indicators.Add(indicator);
				indicator.SetUp();

				VitVWAP[] tmp = new VitVWAP[cacheVitVWAP == null ? 1 : cacheVitVWAP.Length + 1];
				if (cacheVitVWAP != null)
					cacheVitVWAP.CopyTo(tmp, 0);
				tmp[tmp.Length - 1] = indicator;
				cacheVitVWAP = tmp;
				return indicator;
			}
		}
	}
}

namespace NinjaTrader.MarketAnalyzer {
	public partial class Column : ColumnBase {
		[Gui.Design.WizardCondition("Indicator")]
		public Indicator.VitVWAP VitVWAP() {
			return _indicator.VitVWAP(Input);
		}

		public Indicator.VitVWAP VitVWAP(Data.IDataSeries input) {
			return _indicator.VitVWAP(input);
		}
	}
}

namespace NinjaTrader.Strategy {
	public partial class Strategy : StrategyBase {
		[Gui.Design.WizardCondition("Indicator")]
		public Indicator.VitVWAP VitVWAP() {
			return _indicator.VitVWAP(Input);
		}

		public Indicator.VitVWAP VitVWAP(Data.IDataSeries input) {
			if (InInitialize && input == null)
				throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

			return _indicator.VitVWAP(input);
		}
	}
}
#endregion
