# VWAP indicator for NinjaTrader 7
###### Vitalij's well-rounded VWAP indicator for NT7.

## Download
### You can get *newest version* and all *earlier releases* of this indicator in the **[download section](//bitbucket.org/ninjatrader/vwap/downloads)**.

***

![a picture is worth a thousand words](//bytebucket.org/ninjatrader/vwap/raw/master/img/indicator.png)
![settings](//bytebucket.org/ninjatrader/vwap/raw/master/img/settings.png)

## Step by Step
1. [Download Indicator](//bitbucket.org/ninjatrader/vwap/downloads)
1. [Import Indicator](Import)
1. [Configure Indicator](Indicator)

## Links
+ [Source Code](//bitbucket.org/ninjatrader/vwap/src/master/Indicator/VitVWAP.cs) ([Raw](//bitbucket.org/ninjatrader/vwap/raw/master/Indicator/VitVWAP.cs))
+ [Changelog](//bitbucket.org/ninjatrader/vwap/src/master/CHANGELOG.md)
+ [Commit History](//bitbucket.org/ninjatrader/vwap/commits)
+ [Other Indicators](http://ninjatrader.bitbucket.org/)

## Contact
#### You've found a bug, have suggestions or feedback? **[Create a Ticket!](https://bitbucket.org/ninjatrader/vwap/issues/new)**

## Release History
| Version | Date              |
| ------- |:----------------- |:------
| 1.0.0   | 10 September 2013 | initial release

## License
[The MIT License (MIT)](http://opensource.org/licenses/mit-license.php)

Copyright © 2013-2014 <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
